from modeltranslation.translator import TranslationOptions
from modeltranslation.decorators import register
from wagtailstreamforms.models.form import Form

@register(Form)
class FormsTR(TranslationOptions):
    fields = (
        "title",
        "sub_title",
        "form_description",
        "submit_button_text",
        "success_message",
        "error_message",
    )